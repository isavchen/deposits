import 'package:digital_card_v2/models/deposits.dart';
import 'package:equatable/equatable.dart';

class CalculatorState extends Equatable {
  final String currencies;
  final String depositAmount;
  final String periods;
  final bool reinvestInterest;
  final List<Deposits> deposits;
  final bool depositAmountError;

  const CalculatorState(
      {this.currencies,
      this.depositAmount,
      this.periods,
      this.reinvestInterest,
      this.deposits,
      this.depositAmountError});

  @override
  List<Object> get props => [
        currencies,
        depositAmount,
        periods,
        reinvestInterest,
        deposits,
        depositAmountError
      ];

  CalculatorState copyWith({
    String currencies,
    String depositAmount,
    String periods,
    List<Deposits> deposits,
    bool reinvestInterest,
    bool depositAmountError,
  }) {
    return CalculatorState(
      currencies: currencies ?? this.currencies,
      depositAmount: depositAmount ?? this.depositAmount,
      periods: periods ?? this.periods,
      reinvestInterest: reinvestInterest ?? this.reinvestInterest,
      deposits: deposits ?? this.deposits,
      depositAmountError: depositAmountError ?? this.depositAmountError,
    );
  }
}
