import 'dart:math';
import 'package:digital_card_v2/cubit/calculator_state.dart';
import 'package:digital_card_v2/mixins/validation_mixin.dart';
import 'package:digital_card_v2/models/deposits.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CalculatorCubit extends Cubit<CalculatorState> with ValidationMixin {
  CalculatorCubit() : super(const CalculatorState());

  void getDefault(List<Deposits> deposits) {
    List<Deposits> defaultList = _getSortDepositsList(deposits, 'UAH', 'P6M');
    emit(state.copyWith(
      currencies: 'UAH',
      depositAmount: '10000',
      periods: 'P6M',
      reinvestInterest: true,
      depositAmountError: false,
      deposits: defaultList,
    ));
  }

  void getDefaultStateForDetail(List<Deposits> dep, String amount, String currencies, String period) {
    emit(state.copyWith(
      deposits: dep,
      depositAmount: amount,
      currencies: currencies,
      periods: period,
    ));
  }

  void setCurrencies(String code, List<Deposits> deposits) {
    List<Deposits> depList =
        _getSortDepositsList(deposits, code, state.periods);
    emit(state.copyWith(currencies: code, deposits: depList));
    validateDepositAmout(state.depositAmount);
  }

  void setCurrenciesForeDetail(String code) {
    emit(state.copyWith(currencies: code));
  }

  void validateDepositAmout(String amount) {
    double minDepAmount = getMinAmount();
    if (this.validateDepositAmount(amount, minDepAmount))
      emit(state.copyWith(depositAmount: amount, depositAmountError: true));
    else
      emit(state.copyWith(depositAmount: amount, depositAmountError: false));
  }

  void setDepositPeriods(String periods, List<Deposits> deposits) {
    List<Deposits> depList =
        _getSortDepositsList(deposits, state.currencies, periods);
    emit(state.copyWith(periods: periods, deposits: depList));
    validateDepositAmout(state.depositAmount);
  }

  void setReinvestInterest(bool flag) {
    emit(state.copyWith(reinvestInterest: flag));
  }

  double getMinAmount() {
    List<double> list = [];
    state.deposits.forEach((i) {
      i.currencies.forEach((j) {
        if (j.code == state.currencies)
          list.add(double.parse(j.minAmount.toString()));
      });
    });
    return list.reduce(min);
  }

  List<Deposits> _getSortDepositsList(
      List<Deposits> loadedDepositsList, String currencies, String period) {
    List<Deposits> list = [];
    for (int i = 0; i < loadedDepositsList.length; i++) {
      for (int j = 0; j < loadedDepositsList[i].currencies.length; j++) {
        if (loadedDepositsList[i].currencies[j].code == currencies) {
          for (int k = 0;
              k < loadedDepositsList[i].currencies[j].periods.length;
              k++) {
            if (loadedDepositsList[i].currencies[j].periods[k].code == period)
              list.add(loadedDepositsList[i]);
          }
        }
      }
    }
    return list;
  }

  String calculateIncomeAmount(Deposits deposit) {
    int n;
    String incomeAmount;
    double interestRate;
    String period = state.periods ?? 'P6M';
    String currencies = state.currencies ?? 'UAH';
    List<String> parsePeriod = period.split('');

    if (state.reinvestInterest ?? true) {
      parsePeriod[2] == 'M'
          ? n = int.parse(parsePeriod[1])
          : n = int.parse(parsePeriod[1]) * 12;
    } else {
      parsePeriod[2] == 'M'
          ? n = int.parse(parsePeriod[1]) * 30
          : n = int.parse(parsePeriod[1]) * 365;
    }

    deposit.currencies.forEach((j) {
      if (j.code == currencies) {
        j.periods.forEach((k) {
          if (k.code == period) {
            if (k.interestRates.length == 2) {
              if (double.parse(state.depositAmount ?? '10000') >=
                      k.interestRates[0].minAmount &&
                  double.parse(state.depositAmount ?? '10000') <
                      k.interestRates[1].minAmount)
                interestRate =
                    double.parse(k.interestRates[0].value.toString());
              else if (double.parse(state.depositAmount ?? '10000') >=
                  k.interestRates[1].minAmount)
                interestRate =
                    double.parse(k.interestRates[1].value.toString());
            } else if (k.interestRates.length == 1) {
              interestRate = double.parse(k.interestRates[0].value.toString());
            }
          }
        });
      }
    });

    state.reinvestInterest ?? true
        ? incomeAmount = (double.parse(state.depositAmount ?? '10000') *
                pow(1 + (interestRate * 30) / (100 * 365), n))
            .toStringAsFixed(2)
        : incomeAmount = (double.parse(state.depositAmount ?? '10000') +
                ((double.parse(state.depositAmount ?? '10000') *
                        interestRate *
                        n) /
                    365 /
                    100))
            .toStringAsFixed(2);

    return incomeAmount;
  }

  List<String> getPeriodsListByCurrencies(List<Deposits> deposits) {
    List<String> list = [];
    List<String> sortList;
    deposits.forEach((i) {
      i.currencies.forEach((j) {
        if (j.code == state.currencies)
          j.periods.forEach((k) {
            if (!list.contains(k.code)) list.add(k.code);
          });
      });
    });
    list.sort();
    sortList = list.where((element) => element.split('')[2] == 'M').toList();
    sortList += list.where((element) => element.split('')[2] == 'Y').toList();
    return sortList;
  }
}
