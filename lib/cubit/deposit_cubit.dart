import 'package:digital_card_v2/cubit/deposit_state.dart';
import 'package:digital_card_v2/models/deposits.dart';
import 'package:digital_card_v2/services/deposits_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DepositCubit extends Cubit<DepositState> {
  final DepositsRepository depositsRepository;

  DepositCubit(this.depositsRepository) : super(DepositLoadingState());

  Future<void> fetchDeposits() async {
    try {
      final List<Deposits> _loadedDepositsList =
          await depositsRepository.getAllDeposits();
      emit(DepositLoadedState(loadedDeposit: _loadedDepositsList));
    } catch (_) {
      emit(DepositErrorState());
    }
  }

}
