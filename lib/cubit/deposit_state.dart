import 'package:flutter/material.dart';

abstract class DepositState {}

class DepositEmptyState extends DepositState {}

class DepositLoadingState extends DepositState {}

class DepositLoadedState extends DepositState {
  List<dynamic> loadedDeposit;
  DepositLoadedState({@required this.loadedDeposit})
      : assert(loadedDeposit != null);
}

class DepositErrorState extends DepositState {}

