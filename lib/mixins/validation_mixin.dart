mixin ValidationMixin {
  bool validateDepositAmount(String amount, double minDepAmount) {
    if (double.parse(amount) < minDepAmount)
      return true;
    else
      return false;
  }
}
