import 'package:flutter/material.dart';

abstract class Styles {
  static const TextStyle title = TextStyle(
    fontSize: 20,
    fontFamily: 'Futura',
    fontWeight: FontWeight.w500,
    color: Color.fromRGBO(0, 0, 0, 0.87),
  );

  static const TextStyle title2 = TextStyle(
    fontSize: 20,
    fontFamily: 'Futura',
    fontWeight: FontWeight.w500,
    color: Color(0xFF111E29),
  );

  static const TextStyle titleLight = TextStyle(
    fontSize: 20,
    fontFamily: 'Futura',
    fontWeight: FontWeight.w400,
    color: Color(0xFF111E29),
  );

  static const TextStyle headline = TextStyle(
    fontSize: 18,
    fontFamily: 'Futura',
    fontWeight: FontWeight.w400,
    color: Color(0xFF111E29),
  );

  static const TextStyle headlineBold = TextStyle(
    fontSize: 18,
    fontFamily: 'Futura',
    fontWeight: FontWeight.w500,
    color: Color(0xFF111E29),
  );

  static const TextStyle headlineWhiteBold = TextStyle(
    fontSize: 18,
    fontFamily: 'Futura',
    fontWeight: FontWeight.w500,
    color: Colors.white,
  );

  static const TextStyle subTitle = TextStyle(
    fontSize: 16,
    fontFamily: 'Futura',
    fontWeight: FontWeight.w400,
    color: Color(0xFF111E29),
  );

  static const TextStyle subTitleLight = TextStyle(
    fontSize: 16,
    fontFamily: 'Futura',
    fontWeight: FontWeight.w400,
    color: Color.fromRGBO(17, 30, 41, 0.6),
  );

  static const TextStyle subheadline = TextStyle(
    fontSize: 14,
    fontFamily: 'Futura',
    fontWeight: FontWeight.w400,
    color: Color(0xFF111E29),
  );

  static const TextStyle subheadlineGrey = TextStyle(
    fontSize: 14,
    fontFamily: 'Futura',
    fontWeight: FontWeight.w400,
    color: Color(0xFF70787F),
  );

  static const Color borderColor = Color.fromRGBO(92, 92, 92, 0.07);

  static const Color shadowColor = Color.fromRGBO(0, 0, 0, 0.05);

  static const Color buttonColor = Color.fromRGBO(38, 207, 199, 0.1);

  static const Color tiffanyColor = Color(0xFF26CFC7);

  static OutlineInputBorder inputBorder = OutlineInputBorder(
    borderSide: BorderSide(
      color: Color(0xFF70787F).withOpacity(0.2),
    ),
    borderRadius: BorderRadius.circular(12),
  );

  static Decoration backgroundGradient = BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: [
        Color(0xFFF4F5F8),
        Color(0xFFFFFFFF),
      ],
    ),
  );
}
