import 'package:digital_card_v2/models/deposits.dart';
import 'package:digital_card_v2/services/deposits_api_provider.dart';

class DepositsRepository {
  DepositsProvider _depositsProvider = DepositsProvider();
  Future<List<Deposits>> getAllDeposits() => _depositsProvider.getDeposits();
}
