import 'dart:convert';

import 'package:digital_card_v2/models/deposits.dart';
import 'package:flutter/services.dart';

class DepositsProvider {

  Future<List<Deposits>> getDeposits() async {
    final response = await rootBundle.loadString('assets/deposits.json');
    final List<dynamic> data = json.decode(response);
    return data.map((e) => Deposits.fromJson(e)).toList();
  }
}