class Deposits {
  String id;
  List<Currencies> currencies;
  bool autoProlongationAllowed;
  bool reinvestInterest;
  String name;
  String description;
  TopupAmount topupAmount;
  List<String> interestPayment;

  Deposits(
      {this.id,
      this.currencies,
      this.autoProlongationAllowed,
      this.reinvestInterest,
      this.name,
      this.description,
      this.topupAmount,
      this.interestPayment});

  Deposits.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['currencies'] != null) {
      currencies = [];
      json['currencies'].forEach((v) {
        currencies.add(Currencies.fromJson(v));
      });
    }
    autoProlongationAllowed = json['autoProlongationAllowed'];
    reinvestInterest = json['reinvestInterest'];
    name = json['name'];
    description = json['description'];
    topupAmount = json['topupAmount'] != null
        ? TopupAmount.fromJson(json['topupAmount'])
        : null;
    interestPayment = json['interestPayment'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    if (this.currencies != null) {
      data['currencies'] = this.currencies.map((v) => v.toJson()).toList();
    }
    data['autoProlongationAllowed'] = this.autoProlongationAllowed;
    data['reinvestInterest'] = this.reinvestInterest;
    data['name'] = this.name;
    data['description'] = this.description;
    if (this.topupAmount != null) {
      data['topupAmount'] = this.topupAmount.toJson();
    }
    data['interestPayment'] = this.interestPayment;
    return data;
  }
}

class Currencies {
  dynamic minAmount;
  dynamic maxAmount;
  String code;
  String maxPeriod;
  List<Periods> periods;

  Currencies(
      {this.minAmount,
      this.maxAmount,
      this.code,
      this.maxPeriod,
      this.periods});

  Currencies.fromJson(Map<String, dynamic> json) {
    minAmount = json['minAmount'];
    maxAmount = json['maxAmount'];
    code = json['code'];
    maxPeriod = json['maxPeriod'];
    if (json['periods'] != null) {
      periods = [];
      json['periods'].forEach((v) {
        periods.add(Periods.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['minAmount'] = this.minAmount;
    data['maxAmount'] = this.maxAmount;
    data['code'] = this.code;
    data['maxPeriod'] = this.maxPeriod;
    if (this.periods != null) {
      data['periods'] = this.periods.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Periods {
  String code;
  List<InterestRates> interestRates;

  Periods({this.code, this.interestRates});

  Periods.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    if (json['interestRates'] != null) {
      interestRates = [];
      json['interestRates'].forEach((v) {
        interestRates.add(InterestRates.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['code'] = this.code;
    if (this.interestRates != null) {
      data['interestRates'] =
          this.interestRates.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InterestRates {
  dynamic minAmount;
  dynamic value;

  InterestRates({this.minAmount, this.value});

  InterestRates.fromJson(Map<String, dynamic> json) {
    minAmount = json['minAmount'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['minAmount'] = this.minAmount;
    data['value'] = this.value;
    return data;
  }
}

class TopupAmount {
  List<TopupCurrencies> currencies;
  String description;

  TopupAmount({this.currencies, this.description});

  TopupAmount.fromJson(Map<String, dynamic> json) {
    if (json['currencies'] != null) {
      currencies = [];
      json['currencies'].forEach((v) {
        currencies.add(TopupCurrencies.fromJson(v));
      });
    }
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.currencies != null) {
      data['currencies'] = this.currencies.map((v) => v.toJson()).toList();
    }
    data['description'] = this.description;
    return data;
  }
}

class TopupCurrencies {
  dynamic minAmount;
  dynamic maxAmount;
  String code;

  TopupCurrencies({this.minAmount, this.maxAmount, this.code});

  TopupCurrencies.fromJson(Map<String, dynamic> json) {
    minAmount = json['minAmount'];
    maxAmount = json['maxAmount'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['minAmount'] = this.minAmount;
    data['maxAmount'] = this.maxAmount;
    data['code'] = this.code;
    return data;
  }
}
