import 'dart:io';

import 'package:digital_card_v2/cubit/calculator_cubit.dart';
import 'package:digital_card_v2/cubit/calculator_state.dart';
import 'package:digital_card_v2/models/deposits.dart';
import 'package:digital_card_v2/styles/styles.dart';
import 'package:digital_card_v2/widgets/currencies_button.dart';
import 'package:digital_card_v2/widgets/deposit_period.dart';
import 'package:digital_card_v2/widgets/interest_capitalization.dart';
import 'package:digital_card_v2/widgets/selected_deposits.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

class CalculatorScreen extends StatefulWidget {
  final List<Deposits> listAllDeposits;

  CalculatorScreen({this.listAllDeposits});
  @override
  _CalculatorScreenState createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen>
    with WidgetsBindingObserver {
  final _textController = TextEditingController(text: '10000');
  final double targetElevation = 3;
  double _elevation = 0;
  ScrollController _scrollController;
  bool _buttonPressed = false;
  OverlayEntry overlayEntry;

  void _scrollListener() {
    double newElevation = _scrollController.offset > 1 ? targetElevation : 0;
    if (_elevation != newElevation) {
      setState(() {
        _elevation = newElevation;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
    _scrollController?.removeListener(_scrollListener);
    _scrollController?.dispose();
    _textController.dispose();
  }

  @override
  void didChangeMetrics() {
    if (Platform.isIOS) {
      if (WidgetsBinding.instance.window.viewInsets.bottom > 0.0) {
        showDoneButton();
      } else {
        setState(() {
          _buttonPressed = false;
        });
        removeDoneButton();
      }
    }
  }

  Widget keyboardDoneButton() {
    return Container(
      color: Color(0xFFF4F5F8),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          CupertinoButton(
            onPressed: () {
              FocusScope.of(context).requestFocus(FocusNode());
              setState(() {
                _buttonPressed = true;
              });
            },
            padding: EdgeInsets.only(top: 5.0, bottom: 5.0, right: 15.0),
            child: Text('Done',
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: 14,
                    fontWeight: FontWeight.bold)),
          ),
        ],
      ),
    );
  }

  showDoneButton() {
    if (overlayEntry != null) return;
    OverlayState overlayState = Overlay.of(context);
    overlayEntry = OverlayEntry(builder: (context) {
      return Positioned(
          bottom: MediaQuery.of(context).viewInsets.bottom,
          right: 0.0,
          left: 0.0,
          child: keyboardDoneButton());
    });
    overlayState.insert(overlayEntry);
  }

  removeDoneButton() {
    if (overlayEntry != null) {
      overlayEntry.remove();
      overlayEntry = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CalculatorCubit>(
      create: (context) =>
          CalculatorCubit()..getDefault(widget.listAllDeposits),
      child: BlocBuilder<CalculatorCubit, CalculatorState>(
        builder: (context, state) {
          if (_buttonPressed) {
            context
                .read<CalculatorCubit>()
                .validateDepositAmout(_textController.text);
          }
          return GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
              context
                  .read<CalculatorCubit>()
                  .validateDepositAmout(_textController.text);
            },
            child: Container(
              decoration: Styles.backgroundGradient,
              child: Scaffold(
                appBar: AppBar(
                  backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                  leading: BackButton(
                    color: Colors.black,
                  ),
                  elevation: _elevation,
                  centerTitle: true,
                  title: Text(
                    "Депозитний калькулятор",
                    style: Styles.title,
                  ),
                ),
                body: ListView(
                  shrinkWrap: true,
                  controller: _scrollController,
                  children: [
                    CurrenciesButton(widget.listAllDeposits),
                    Padding(
                      padding: EdgeInsets.only(left: 16, top: 12, right: 16),
                      child: TextFormField(
                        controller: _textController,
                        keyboardType: TextInputType.number,
                        onFieldSubmitted: (val) => context
                            .read<CalculatorCubit>()
                            .validateDepositAmout(val),
                        style: Styles.titleLight,
                        decoration: InputDecoration(
                          labelText: "Сума депозиту",
                          suffixText: state.currencies == 'UAH'
                              ? '₴'
                              : state.currencies == 'USD'
                                  ? '\$'
                                  : '€',
                          labelStyle: state.depositAmountError
                              ? Styles.subheadlineGrey
                                  .copyWith(color: Colors.red)
                              : Styles.subheadlineGrey,
                          suffixStyle: Styles.subheadlineGrey,
                          border: state.depositAmountError
                              ? Styles.inputBorder.copyWith(
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                  ),
                                )
                              : Styles.inputBorder,
                          errorBorder: state.depositAmountError
                              ? Styles.inputBorder.copyWith(
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                  ),
                                )
                              : Styles.inputBorder,
                          enabledBorder: state.depositAmountError
                              ? Styles.inputBorder.copyWith(
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                  ),
                                )
                              : Styles.inputBorder,
                          focusedBorder: state.depositAmountError
                              ? Styles.inputBorder.copyWith(
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                  ),
                                )
                              : Styles.inputBorder,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: Center(
                        child: Text(
                          'Мінімальна сума для відкриття депозиту ' +
                              '${context.read<CalculatorCubit>().getMinAmount().toString()}' +
                              '${state.currencies == 'UAH' ? '₴' : state.currencies == 'USD' ? '\$' : '€'}',
                          style: state.depositAmountError
                              ? Styles.subheadlineGrey
                                  .copyWith(color: Colors.red)
                              : Styles.subheadlineGrey,
                        ),
                      ),
                    ),
                    DepositPeriod(listAllDeposits: widget.listAllDeposits),
                    InterestCapitalization(),
                    if (!state.depositAmountError)
                      Padding(
                        padding: EdgeInsets.only(left: 16, top: 40, bottom: 4),
                        child: Text(
                          "Підібрані депозити",
                          style: Styles.headlineBold,
                        ),
                      ),
                    if (!state.depositAmountError)
                      Column(
                        children: Iterable<int>.generate(state.deposits.length)
                            .map((index) {
                          return SelectedDeposit(
                              depositName: state.deposits[index].name,
                              minTerm: state.deposits[index].currencies
                                  .where((element) =>
                                      element.code == state.currencies)
                                  .toList()[0]
                                  .periods[0]
                                  .code,
                              maxTerm: state.deposits[index].currencies
                                  .where((element) =>
                                      element.code == state.currencies)
                                  .toList()[0]
                                  .maxPeriod,
                              index: index);
                        }).toList(),
                      ),
                    if (!state.depositAmountError)
                      Padding(
                        padding: EdgeInsets.only(top: 20, bottom: 40),
                        child: GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset("assets/img/deposit.svg"),
                              SizedBox(width: 16),
                              Text(
                                "Переглянути усі депозити",
                                style: Styles.headline,
                              ),
                            ],
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
