import 'dart:math';

import 'package:digital_card_v2/models/deposits.dart';
import 'package:digital_card_v2/styles/styles.dart';
import 'package:digital_card_v2/widgets/deposit_detail_card.dart';
import 'package:digital_card_v2/widgets/row_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DepositDetails extends StatefulWidget {
  final Deposits deposit;
  final String currencies;
  final String depositAmount;

  DepositDetails({this.deposit, this.currencies, this.depositAmount});

  @override
  _DepositDetailsState createState() => _DepositDetailsState();
}

class _DepositDetailsState extends State<DepositDetails> {
  final double targetElevation = 3;
  String currenciesState;
  double _elevation = 0;
  ScrollController _controller;

  void _scrollListener() {
    double newElevation = _controller.offset > 1 ? targetElevation : 0;
    if (_elevation != newElevation) {
      setState(() {
        _elevation = newElevation;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    setState(() {
      currenciesState = widget.currencies;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller?.removeListener(_scrollListener);
    _controller?.dispose();
  }

  Widget build(BuildContext context) {
    return Container(
      decoration: Styles.backgroundGradient,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          leading: BackButton(
            color: Colors.black,
          ),
          elevation: _elevation,
          centerTitle: true,
          title: Text(
            widget.deposit.name,
            style: Styles.title,
          ),
        ),
        body: Column(
          children: [
            Expanded(
              child: ListView(
                controller: _controller,
                shrinkWrap: true,
                children: [
                  RowButton(
                    currencies: currenciesState,
                    deposit: widget.deposit,
                    changeCurrencies: (String newCurrencies) {
                      setState(() {
                        currenciesState = newCurrencies;
                      });
                    },
                  ),
                  DepositDetailCard(
                    deposit: widget.deposit,
                    currencies: currenciesState,
                  ),
                  Center(
                    child: Text(
                      'Мінімальна сума для відкриття депозиту ' +
                          '${getMinAmount(widget.deposit, currenciesState)}' +
                          '${currenciesState == 'UAH' ? '₴' : currenciesState == 'USD' ? '\$' : '€'}',
                      style: Styles.subTitleLight,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(
                              width: 1,
                              color: Styles.borderColor,
                            ),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 14, vertical: 15.09),
                            child:
                                SvgPicture.asset("assets/img/donation_dep.svg"),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 12),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Поповнення депозиту",
                                  style: Styles.headline,
                                ),
                                SizedBox(height: 4),
                                if (widget.deposit.topupAmount.currencies
                                    .where((element) =>
                                        element.code == currenciesState)
                                    .isNotEmpty)
                                  Text(
                                    'Можливо від ' +
                                        '${widget.deposit.topupAmount.currencies.where((element) => element.code == currenciesState).toList()[0].minAmount}' +
                                        '${currenciesState == 'UAH' ? '₴' : currenciesState == 'USD' ? '\$' : '€'}',
                                    style: Styles.subheadlineGrey,
                                  )
                              ],
                            ),
                          ),
                        ),
                        Text(
                          widget.deposit.topupAmount.currencies
                                  .where((i) => i.code == currenciesState)
                                  .isNotEmpty
                              ? 'Так'
                              : 'Ні',
                          style: Styles.title2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(
                              width: 1,
                              color: Styles.borderColor,
                            ),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 14, vertical: 15.09),
                            child: SvgPicture.asset("assets/img/clock.svg"),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 12),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Автопродовження", style: Styles.headline),
                                SizedBox(height: 4),
                                Text(
                                  "Автоматичне продовження депозиту",
                                  style: Styles.subheadlineGrey,
                                )
                              ],
                            ),
                          ),
                        ),
                        Text(
                          widget.deposit.currencies
                                      .where((i) => i.code == currenciesState)
                                      .isNotEmpty &&
                                  widget.deposit.autoProlongationAllowed
                              ? "Так"
                              : 'Ні',
                          style: Styles.title2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(
                              width: 1,
                              color: Styles.borderColor,
                            ),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 14, vertical: 15.09),
                            child: SvgPicture.asset("assets/img/cancel.svg"),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 12),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Дострокове розірвання",
                                  style: Styles.headline,
                                ),
                                SizedBox(height: 4),
                                Text(
                                  "Без відсотків за останній місяць",
                                  style: Styles.subheadlineGrey,
                                )
                              ],
                            ),
                          ),
                        ),
                        Text(
                          "Ні",
                          style: Styles.title2,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 38, bottom: 22),
                    child: GestureDetector(
                      onTap: () {
                        //urllauncher
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset("assets/img/info.svg"),
                          SizedBox(width: 16),
                          Text(
                            "Детальна інформація",
                            style: Styles.headline,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: Container(
                  decoration: BoxDecoration(
                    color: Styles.tiffanyColor,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0),
                      child: Text(
                        "ПЕРЕЙТИ ДО ОФОРМЛЕННЯ",
                        style: Styles.headlineWhiteBold,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

String getMinAmount(Deposits dep, String currencies) {
  List<double> list = [];
  dep.currencies.forEach((j) {
    if (j.code == currencies) list.add(double.parse(j.minAmount.toString()));
  });
  if (list.isEmpty) return null;
  return list.reduce(min).toString();
}
