import 'package:digital_card_v2/cubit/deposit_cubit.dart';
import 'package:digital_card_v2/services/deposits_repository.dart';
import 'package:digital_card_v2/styles/styles.dart';
import 'package:digital_card_v2/widgets/calculator_button.dart';
import 'package:digital_card_v2/widgets/deposits_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DepositsScreen extends StatefulWidget {
  @override
  _DepositsScreenState createState() => _DepositsScreenState();
}

class _DepositsScreenState extends State<DepositsScreen> {
  final depositsRepository = DepositsRepository();
  final double targetElevation = 3;
  double _elevation = 0;
  ScrollController _controller;

  void _scrollListener() {
    double newElevation = _controller.offset > 1 ? targetElevation : 0;
    if (_elevation != newElevation) {
      setState(() {
        _elevation = newElevation;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  @override
  void dispose() {
    super.dispose();
    _controller?.removeListener(_scrollListener);
    _controller?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: Styles.backgroundGradient,
      child: Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: _elevation,
          backgroundColor: Colors.white,
          title: Text(
            "Депозити",
            style: Styles.title,
          ),
        ),
        body: BlocProvider<DepositCubit>(
          create: (context) =>
              DepositCubit(depositsRepository)..fetchDeposits(),
          child: ListView(
            shrinkWrap: true,
            controller: _controller,
            children: [
              Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 26, vertical: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // SvgPicture.asset('assets/img/image111.svg'),
                      Image.asset('assets/img/image111.png'),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16),
                          child: Text(
                            "Ощадбанк учасник Фонду гарантованих вкладів фізичних осіб",
                            style: Styles.subTitle,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              CalculatorButton(),
              DepositsList(),
              SizedBox(height: 16),
            ],
          ),
        ),
      ),
    );
  }
}
