import 'package:digital_card_v2/cubit/calculator_cubit.dart';
import 'package:digital_card_v2/cubit/calculator_state.dart';
import 'package:digital_card_v2/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class InterestCapitalization extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CalculatorCubit, CalculatorState>(
        builder: (context, state) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                border: Border.all(
                  width: 1,
                  color: Styles.borderColor,
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 14, vertical: 15.09),
                child: SvgPicture.asset("assets/img/percent.svg"),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Капіталізація відсотків",
                      style: Styles.headline,
                    ),
                    SizedBox(height: 6),
                    Text(
                      "Відсоток по депозиту буде вище",
                      style: Styles.subheadlineGrey,
                    )
                  ],
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color:
                    state.reinvestInterest ? Styles.tiffanyColor : Colors.grey,
              ),
              height: 30,
              width: 54,
              child: Transform.scale(
                scale: 1.1,
                child: Switch(
                  value: state.reinvestInterest,
                  onChanged: (value) {
                    context.read<CalculatorCubit>().setReinvestInterest(value);
                  },
                  activeTrackColor: Colors.transparent,
                  activeColor: Colors.white,
                  inactiveThumbColor: Colors.white,
                  inactiveTrackColor: Colors.transparent,
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
