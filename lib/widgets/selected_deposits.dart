import 'package:digital_card_v2/cubit/calculator_cubit.dart';
import 'package:digital_card_v2/cubit/calculator_state.dart';
import 'package:digital_card_v2/screens/deposit_detail.dart';
import 'package:digital_card_v2/styles/styles.dart';
import 'package:digital_card_v2/widgets/deposits_card.dart';
import 'package:digital_card_v2/widgets/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SelectedDeposit extends StatelessWidget {
  final String depositName;
  final String minTerm;
  final String maxTerm;
  final int index;

  SelectedDeposit({
    @required this.depositName,
    @required this.minTerm,
    @required this.maxTerm,
    @required this.index,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CalculatorCubit, CalculatorState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(left: 16, top: 8, right: 16),
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DepositDetails(
                    deposit: state.deposits
                        .where((i) => i.name == depositName)
                        .toList()[0],
                    currencies: state.currencies,
                    depositAmount: state.depositAmount,
                  ),
                ),
              );
            },
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                border: Border.all(
                  width: 1,
                  color: Styles.borderColor,
                ),
                boxShadow: [
                  BoxShadow(
                    color: Styles.shadowColor,
                    blurRadius: 10,
                    offset: Offset(0, 2),
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16, 20, 16, 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      depositName,
                      style: Styles.headline,
                    ),
                    SizedBox(height: 4),
                    Text(
                      periodFormat(minTerm, maxTerm),
                      style: Styles.subheadlineGrey,
                    ),
                    SizedBox(height: 4),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          "Дохід",
                          style: Styles.subheadlineGrey,
                        ),
                        SizedBox(width: 4),
                        Expanded(
                          child: DottedLine(
                            color: Color(0xFF70787F),
                          ),
                        ),
                        SizedBox(width: 4),
                        RichText(
                          text: TextSpan(
                            text: context
                                .read<CalculatorCubit>()
                                .calculateIncomeAmount(state.deposits[index])
                                .split('.')[0],
                            style: Styles.headlineBold,
                            children: <TextSpan>[
                              TextSpan(
                                text: "." +
                                    context
                                        .read<CalculatorCubit>()
                                        .calculateIncomeAmount(
                                            state.deposits[index])
                                        .split('.')[1],
                                style: Styles.subheadline,
                              ),
                              TextSpan(
                                text: state.currencies == 'UAH'
                                    ? '₴'
                                    : state.currencies == 'USD'
                                        ? '\$'
                                        : '€',
                                style: Styles.subheadline,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
