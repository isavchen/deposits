import 'package:digital_card_v2/models/deposits.dart';
import 'package:digital_card_v2/screens/deposit_detail.dart';
import 'package:digital_card_v2/styles/styles.dart';
import 'package:digital_card_v2/widgets/deposit_period.dart';
import 'package:flutter/material.dart';

class DepositDetailCard extends StatelessWidget {
  final Deposits deposit;
  final String currencies;

  DepositDetailCard({this.deposit, this.currencies});

  @override
  Widget build(BuildContext context) {
        Currencies currencies = this.deposit.currencies
            .where((i) => i.code == this.currencies)
            .first;
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 20, 26, 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                          Text(
                            "Сума депозиту",
                            style: Styles.subTitleLight,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ] +
                        Iterable<int>.generate(currencies.periods.length)
                            .map((index) {
                          return Column(
                            children: [
                              Text(
                                selectPeriodFormat(
                                    currencies.periods[index].code),
                                style: Styles.subTitleLight,
                              ),
                              if (index < currencies.periods.length - 1)
                                SizedBox(
                                  height: 20,
                                ),
                            ],
                          );
                        }).toList(),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                          RichText(
                            text: TextSpan(
                                text: getMinAmount(this.deposit, this.currencies),
                                style: Styles.headlineBold,
                                children: [
                                  TextSpan(
                                    text: this.currencies == 'UAH'
                                        ? '₴'
                                        : this.currencies == 'USD'
                                            ? '\$'
                                            : '€',
                                    style: Styles.subheadline,
                                  ),
                                ]),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ] +
                        Iterable<int>.generate(currencies.periods.length)
                            .map((index) {
                          return Column(
                            children: [
                              Text(
                                currencies.periods[index].interestRates[0].value
                                        .toString() +
                                    '%',
                                style: Styles.subTitle,
                              ),
                              if (index <
                                  deposit.currencies
                                          .where(
                                              (i) => i.code == this.currencies)
                                          .first
                                          .periods
                                          .length -
                                      1)
                                SizedBox(
                                  height: 20,
                                ),
                            ],
                          );
                        }).toList(),
                  ),
                  if (currencies.periods[0].interestRates.length > 1)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                            RichText(
                              text: TextSpan(
                                  text: currencies
                                      .periods[0].interestRates[1].minAmount
                                      .toString(),
                                  style: Styles.headlineBold,
                                  children: [
                                    TextSpan(
                                      text: this.currencies == 'UAH'
                                          ? '₴'
                                          : this.currencies == 'USD'
                                              ? '\$'
                                              : '€',
                                      style: Styles.subheadline,
                                    ),
                                  ]),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                          ] +
                          Iterable<int>.generate(currencies.periods.length)
                              .map((index) {
                            return Column(
                              children: [
                                Text(
                                  currencies
                                          .periods[index].interestRates[1].value
                                          .toString() +
                                      '%',
                                  style: Styles.subTitle,
                                ),
                                if (index < currencies.periods.length - 1)
                                  SizedBox(
                                    height: 20,
                                  ),
                              ],
                            );
                          }).toList(),
                    ),
                ],
              ),
            ),
          ),
        );
  }
}
