import 'package:digital_card_v2/screens/calculator_screen.dart';
import 'package:digital_card_v2/cubit/calculator_cubit.dart';
import 'package:digital_card_v2/cubit/deposit_cubit.dart';
import 'package:digital_card_v2/cubit/deposit_state.dart';
import 'package:digital_card_v2/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CalculatorButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DepositCubit, DepositState>(builder: (context, state) {
      if (state is DepositLoadingState) {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
      if (state is DepositLoadedState) {
        return Padding(
          padding: EdgeInsets.fromLTRB(16, 20, 16, 0),
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      CalculatorScreen(listAllDeposits: state.loadedDeposit),
                ),
              );
            },
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                border: Border.all(
                  width: 1,
                  color: Styles.borderColor,
                ),
                boxShadow: [
                  BoxShadow(
                    color: Styles.shadowColor,
                    blurRadius: 10,
                    offset: Offset(0, 2),
                  ),
                ],
              ),
              child: Padding(
                padding: EdgeInsets.only(left: 16, top: 16, bottom: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: Colors.white,
                        border: Border.all(
                          width: 1,
                          color: Styles.borderColor,
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(14),
                        child:
                            SvgPicture.asset('assets/img/calculator_icon.svg'),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 12),
                      child: Text(
                        "Депозитний калькулятор",
                        style: Styles.headline,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      }
      return null;
    });
  }
}
