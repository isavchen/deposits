import 'package:digital_card_v2/cubit/calculator_cubit.dart';
import 'package:digital_card_v2/cubit/calculator_state.dart';
import 'package:digital_card_v2/models/deposits.dart';
import 'package:digital_card_v2/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DepositPeriod extends StatelessWidget {
  final List<Deposits> listAllDeposits;

  DepositPeriod({this.listAllDeposits});

  void _showSheet(
      {List<String> list,
      String periodState,
      CalculatorCubit cubit,
      BuildContext context}) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: false, // set this to true
      backgroundColor: Colors.transparent,
      builder: (_) {
        return DraggableScrollableSheet(
          expand: false,
          initialChildSize: 1,
          minChildSize: 0.9,
          builder: (_, controller) {
            return Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 12.0),
                    child: Center(
                      child: Container(
                        height: 4,
                        width: 55,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Color(0xFFEDEFF5),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      "Термін депозиту",
                      style: Styles.title2,
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: list.length,
                      controller: controller, // set this too
                      itemBuilder: (_, i) => ListTile(
                        title: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 10),
                          child: GestureDetector(
                            onTap: () {
                              cubit.setDepositPeriods(
                                  list[i], this.listAllDeposits);
                              Navigator.pop(context);
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(vertical: 12),
                              child: Text(
                                selectPeriodFormat(list[i]),
                                style: list[i] == periodState
                                    ? Styles.headline.copyWith(color: Color(0xFF26CFC7))
                                    : Styles.headline,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CalculatorCubit, CalculatorState>(
        builder: (context, state) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
        child: GestureDetector(
          onTap: () {
            _showSheet(
              list: context
                  .read<CalculatorCubit>()
                  .getPeriodsListByCurrencies(this.listAllDeposits),
              periodState: state.periods,
              cubit: context.read<CalculatorCubit>(),
              context: context,
            );
          },
          child: Container(
            color: Colors.transparent,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(
                      width: 1,
                      color: Styles.borderColor,
                    ),
                  ),
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 14, vertical: 15.09),
                    child: SvgPicture.asset("assets/img/content-icon.svg"),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(selectPeriodFormat(state.periods),
                            style: Styles.headline),
                        SizedBox(height: 6),
                        Text(
                          "Термін депозиту",
                          style: Styles.subheadlineGrey,
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 20.0,
                      horizontal: 7.0,
                    ),
                    child: SvgPicture.asset("assets/img/arrow_down.svg"),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}

String selectPeriodFormat(String period) {
    List<String> formatPeriod = period.split('');
    int number = int.parse(formatPeriod[1]);
    String string;

    if (number == 1) {
      if (formatPeriod[2] == 'M')
        string = 'місяць';
      else
        string = 'рік';
    } else if (number > 1 && number < 5) {
      if (formatPeriod[2] == 'M')
        string = 'місяці';
      else
        string = 'роки';
    } else {
      if (formatPeriod[2] == 'M')
        string = 'місяців';
      else
        string = 'років';
    }
    return '${formatPeriod[1]} $string';
  }
