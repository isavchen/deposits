import 'package:digital_card_v2/cubit/calculator_cubit.dart';
import 'package:digital_card_v2/cubit/calculator_state.dart';
import 'package:digital_card_v2/models/deposits.dart';
import 'package:digital_card_v2/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CurrenciesButton extends StatelessWidget {
  final List<Deposits> listDeposits;
  CurrenciesButton(this.listDeposits);

  @override
  Widget build(BuildContext context) {
    final CalculatorCubit calculatorCubit =
        BlocProvider.of<CalculatorCubit>(context);
    return BlocBuilder<CalculatorCubit, CalculatorState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ButtonWidget(
                  onTap: () {
                    listDeposits == null
                        ? calculatorCubit.setCurrenciesForeDetail('UAH')
                        : calculatorCubit.setCurrencies(
                            'UAH', this.listDeposits);
                  },
                  currencies: 'UAH',
                  stateCurrencies: state.currencies,
                ),
                ButtonWidget(
                  onTap: () {
                    listDeposits == null
                        ? calculatorCubit.setCurrenciesForeDetail('USD')
                        : calculatorCubit.setCurrencies(
                            'USD', this.listDeposits);
                  },
                  currencies: 'USD',
                  stateCurrencies: state.currencies,
                ),
                ButtonWidget(
                  onTap: () {
                    listDeposits == null
                        ? calculatorCubit.setCurrenciesForeDetail('EUR')
                        : calculatorCubit.setCurrencies(
                            'EUR', this.listDeposits);
                  },
                  currencies: 'EUR',
                  stateCurrencies: state.currencies,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class ButtonWidget extends StatelessWidget {
  final Function onTap;
  final String currencies;
  final String stateCurrencies;

  ButtonWidget({this.onTap, this.currencies, this.stateCurrencies});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: 104,
        height: 40,
        decoration: BoxDecoration(
          color:
              stateCurrencies == currencies ? Styles.buttonColor : Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
              color: Styles.shadowColor,
              blurRadius: 10,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: Center(
          child: Text(
            currencies,
            style: Styles.subheadline,
          ),
        ),
      ),
    );
  }
}
