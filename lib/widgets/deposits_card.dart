import 'package:digital_card_v2/models/deposits.dart';
import 'package:digital_card_v2/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DepositsCard extends StatelessWidget {
  final String depositName;
  final String minTerm;
  final String maxTerm;
  final String imgUrl;
  final List<Currencies> currency;
  final String minDepositAmount;
  final String desc3;

  DepositsCard(
      {@required this.depositName,
      @required this.minTerm,
      @required this.maxTerm,
      @required this.imgUrl,
      @required this.currency,
      @required this.minDepositAmount,
      @required this.desc3});

  Widget getTextWidgets(List<Currencies> currencies) {
    List<Widget> list = [];
    for (var i = 0; i < currencies.length; i++) {
      list.add(Text(
        currencies[i].code,
        style: Styles.subTitle,
      ));
      if (i != currency.length - 1)
        list.add(Text(
          ', ',
          style: Styles.subTitle,
        ));
    }
    return new Row(children: list);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(
            width: 1,
            color: Styles.borderColor,
          ),
          boxShadow: [
            BoxShadow(
              color: Styles.shadowColor,
              blurRadius: 10,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 27, 12, 20),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 19),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          depositName,
                          style: Styles.headlineBold,
                        ),
                        SizedBox(height: 8),
                        Text(
                          periodFormat(minTerm, maxTerm),
                          style: Styles.subheadlineGrey,
                        ),
                        // DepositFormat(formatDeposit: minTerm),
                      ],
                    ),
                    Image.asset(imgUrl),
                  ],
                ),
              ),
              DescriptionDeposit(
                description: Row(
                  children: [
                    Text(
                      "Валюта для вкладу ",
                      style: Styles.subTitle,
                    ),
                    getTextWidgets(currency),
                  ],
                ),
              ),
              SizedBox(height: 9),
              DescriptionDeposit(
                description: RichText(
                  text: TextSpan(
                    text: "Поповнення від ",
                    style: Styles.subTitle,
                    children: <TextSpan>[
                      TextSpan(
                        text: minDepositAmount,
                        style: Styles.headlineBold,
                      ),
                      TextSpan(
                        text: ".00₴",
                        style: Styles.subTitle,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 9),
              DescriptionDeposit(
                description: Text(
                  desc3,
                  style: Styles.subTitle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DescriptionDeposit extends StatelessWidget {
  final Widget description;

  DescriptionDeposit({this.description});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 5.33),
          child: SvgPicture.asset('assets/img/checkbox.svg'),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 9.33),
          child: description,
        ),
      ],
    );
  }
}

String periodFormat(String minTerm, String maxTerm) {
  List<String> formatMinTerm = minTerm.split('');
  List<String> formatMaxTerm = maxTerm.split('');
  String startPeriod;
  String endPeriod;
  if (formatMinTerm[2] == 'M')
    startPeriod = formatMinTerm[1];
  else if (formatMinTerm[2] == 'Y')
    startPeriod = (int.parse(formatMinTerm[1]) * 12).toString();
  if (formatMaxTerm[2] == 'M')
    endPeriod = formatMaxTerm[1];
  else if (formatMaxTerm[2] == 'Y')
    endPeriod = (int.parse(formatMaxTerm[1]) * 12).toString();
  return 'Від ' + startPeriod + ' до ' + endPeriod + ' місяців';
}
