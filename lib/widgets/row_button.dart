import 'package:digital_card_v2/models/deposits.dart';
import 'package:digital_card_v2/screens/deposit_detail.dart';
import 'package:digital_card_v2/widgets/currencies_button.dart';
import 'package:flutter/material.dart';

class RowButton extends StatelessWidget {
  final String currencies;
  final Deposits deposit;
  final Function changeCurrencies;

  RowButton({this.currencies, this.changeCurrencies, this.deposit});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ButtonWidget(
              onTap: () {
                if (getMinAmount(deposit, 'UAH') != null)
                  changeCurrencies('UAH');
              },
              currencies: 'UAH',
              stateCurrencies: currencies,
            ),
            ButtonWidget(
              onTap: () {
                if (getMinAmount(deposit, 'USD') != null)
                  changeCurrencies('USD');
              },
              currencies: 'USD',
              stateCurrencies: currencies,
            ),
            ButtonWidget(
              onTap: () {
                if (getMinAmount(deposit, 'EUR') != null)
                  changeCurrencies('EUR');
              },
              currencies: 'EUR',
              stateCurrencies: currencies,
            ),
          ],
        ),
      ),
    );
  }
}
