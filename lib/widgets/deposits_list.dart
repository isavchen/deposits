import 'package:digital_card_v2/cubit/deposit_cubit.dart';
import 'package:digital_card_v2/cubit/deposit_state.dart';
import 'package:digital_card_v2/screens/deposit_detail.dart';
import 'package:digital_card_v2/widgets/deposits_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DepositsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DepositCubit, DepositState>(
      builder: (context, state) {
        if (state is DepositLoadingState) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        if (state is DepositLoadedState) {
          return Column(
            children:
                Iterable<int>.generate(state.loadedDeposit.length).map((index) {
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DepositDetails(deposit: state.loadedDeposit[index], currencies: 'UAH',),
                    ),
                  );
                },
                child: DepositsCard(
                  depositName: state.loadedDeposit[index].name,
                  minTerm: state.loadedDeposit[index].currencies
                      .where((i) => i.code == 'UAH')
                      .toList()[0]
                      .periods[0]
                      .code,
                  maxTerm: state.loadedDeposit[index].currencies
                      .where((i) => i.code == 'UAH')
                      .toList()[0]
                      .maxPeriod,
                  imgUrl: 'assets/img/logo_dep.png',
                  currency: state.loadedDeposit[index].currencies,
                  minDepositAmount: state.loadedDeposit[index].currencies
                      .where((i) => i.code == 'UAH')
                      .toList()[0]
                      .minAmount
                      .toInt()
                      .toString(),
                  desc3: 'Виплата процентів або капіталізація',
                ),
              );
            }).toList(),
          );
        }

        if (state is DepositErrorState) {
          return Center(
            child: Text(
              'Error fetching deposits',
              style: TextStyle(fontSize: 20.0),
            ),
          );
        }
        return null;
      },
    );
  }
}
